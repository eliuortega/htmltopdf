﻿using SelectPdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmltoPdf
{
    class Program : IPrintableTransformationHandler
    {
       

        public byte[] ToPdf(Stream input)
        {
            byte[] output = null;
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                output = ms.ToArray();
            }
            //Console.WriteLine(Encoding.UTF8.GetString(output));
            HtmlToPdf converter = new HtmlToPdf();
            converter.Options.PdfPageSize = PdfPageSize.A4;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
       
            PdfDocument doc = converter.ConvertHtmlString(Encoding.UTF8.GetString(output));
            try
            {
                doc.Save("desdeFileHTML.pdf");
                byte[] bytes = File.ReadAllBytes("desdeFileHTML.pdf");
                doc.Close();
                return bytes;
            }
            finally
            {
                doc.Close();
                Console.WriteLine("Documento creado perfectamente");
            }
        }

        //input
        public byte[] ToPdf(string inputFileFullPath)
        {
            HtmlToPdf converter = new HtmlToPdf();


            PdfDocument doc = converter.ConvertUrl(inputFileFullPath);
            try
            {
                
                doc.Save("Prueba.pdf");

                byte[] bytes = File.ReadAllBytes(inputFileFullPath);
                //

                doc.Close();
                
                return bytes;
            }
            finally
            {
                doc.Close();
                Console.WriteLine("Archivo Creado correctamente");
            }
        }

        //Desde Codigo HTML
        public byte[] ToPdfFromContent(string content)
        {
            byte[] utf8Bytes = System.Text.Encoding.UTF8.GetBytes(content);
            string htmlString = System.Text.Encoding.UTF8.GetString(utf8Bytes);
            string baseUrl = "";

            string pdf_page_size = "A4";
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                pdf_page_size, true);
            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                pdf_orientation, true);
            int webPageWidth = 1024;
            try
            {
                webPageWidth = Convert.ToInt32("1024");
            }
            catch { }

            int webPageHeight = 0;
            try
            {
                webPageHeight = Convert.ToInt32("");
            }
            catch { }
            HtmlToPdf converter = new HtmlToPdf();
            ;
            converter.Options.PdfPageSize = PdfPageSize.A4;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;
            PdfDocument doc = converter.ConvertHtmlString(htmlString, baseUrl);
            try
            {
                doc.Save("desdeTextHTML.pdf");
                byte[] bytes = File.ReadAllBytes("desdeTextHTML.pdf");
                doc.Close();
                return bytes;
            }
            finally
            {
                doc.Close();
                Console.WriteLine("Documento creado perfectamente");
            }
        }



        static void Main(string[] args)
        {
            Program pdf = new Program();
            string path;
            string contenido;
            bool menu = true;
            while (menu)
            {
                Console.WriteLine("SISTEMA DE CREACION DE PDF\nMENU\n");
                Console.WriteLine("1- Creacion leyendo un Archivo");
                Console.WriteLine("2- Creacion desde un archivo HTML");
                Console.WriteLine("3- Creacion manual del HTML");
                Console.WriteLine("4- Salir");
                string res = Console.ReadLine();
                
                switch (res)
                {
                    case "1":
                        Console.WriteLine("Introduce la ruta del Archivo a leer");
                        //@"C:\Users\eliuo\Desktop\HtmlToPdf\HtmltoPdf\result.html";
                        path = Console.ReadLine();
                        if(path == string.Empty)
                        {
                            Console.WriteLine("No puedes dejar la ruta vacia");
                            break;
                        }
                        else
                        {
                            pdf.ToPdf(File.Open(@path, FileMode.Open));
                            break;
                        }
                        
                    case "2":
                        Console.WriteLine("Introduce la ruta del Archivo a leer");
                        path = Console.ReadLine();
                        if(path == string.Empty)
                        {
                            Console.WriteLine("No puedes dejar la ruta vacia");
                            break;
                        }
                        else
                        {
                            pdf.ToPdf(path);
                            break;
                        }
                        
                    case "3":
                        Console.WriteLine("Digita con la Sintaxis HTML y CSS");
                        contenido = Console.ReadLine();
                        if(contenido == string.Empty)
                        {
                            Console.WriteLine("Contenido en blanco");
                            break;
                        }
                        else
                        {
                            pdf.ToPdfFromContent(contenido);
                            break;
                        }
                    case "4":
                        Console.Clear();
                        Console.WriteLine("Saliendo del sistema...");
                        System.Threading.Thread.Sleep(3000);
                        
                        menu = false;
                        break;

                    default:
                        Console.WriteLine("Este Opcion no es validad");
                        
                        break;
                }
            }
        }
    }
}
