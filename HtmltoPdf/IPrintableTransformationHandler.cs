﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmltoPdf
{
    interface IPrintableTransformationHandler
    {
        /// <summary>
        /// Transforms the input stream into its PDF representation. Assumes the stream is reading from an HTML input.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        byte[] ToPdf(Stream input);

        /// <summary>
        /// Transform the given file to its PDF version.
        /// </summary>
        /// <param name="inputFileFullPath"></param>
        /// <returns></returns>
        byte[] ToPdf(string inputFileFullPath);

        /// <summary>
        /// Converts the given HTML string into its PDF representation.
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        byte[] ToPdfFromContent(string content);
    }
}
